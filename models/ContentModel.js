const mongoose = require("mongoose");
const { Schema } = mongoose;
var AutoIncrement = require('mongoose-sequence')(mongoose);

const ContentSchema = new Schema({
    id: {type: Number, unique: true},
    category: String,
    name: String,
    location: String,
    description: String,
    price: String,
    rating: String,
    urlImage: String,
});

ContentSchema.plugin(AutoIncrement,{inc_field: 'id'});

const Content = mongoose.model("content", ContentSchema);

module.exports = Content;