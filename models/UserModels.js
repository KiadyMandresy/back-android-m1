const mongoose = require("mongoose");
const { Schema } = mongoose;
var AutoIncrement = require('mongoose-sequence')(mongoose);

const UserSchema = new Schema({
    idUser: {type: Number, unique: true},
    nom: String,
    prenom: String,
    login: {
        type: String,
        required: true,
        unique: true,
    },
    email:{
        type: String,
        required: true
    }, 
    mdp: {
        type: String,
        required: true
    }
});

UserSchema.plugin(AutoIncrement,{inc_field: 'idUser'});

const User = mongoose.model("users", UserSchema);

module.exports = User;