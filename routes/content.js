const Voiture = require('../models/ContentModel');
var express = require('express');
const controller = require('../controllers/ContentController')
var router = express.Router();

router.get('/getAll',controller.getAll);

router.get('/getById/:id',controller.getById);

router.post('/Update',controller.update);

router.post('/Create',controller.create);

router.get('/find/:filter',controller.getByFilter);

module.exports = router;