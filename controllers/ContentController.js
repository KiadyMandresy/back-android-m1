const Content = require('../models/ContentModel');
const imageHelper = require('../helpers/ImageHelper');

exports.create = async (req, res) => {
    console.log("ligne 5 Create Content");
    console.log(req.body);
    const data = new Content({
        category: req.body.category,
        name: req.body.name,
        location: req.body.location,
        description: req.body.description,
        price: req.body.price,
        rating: req.body.rating,
        urlImage: req.body.urlImage,
        
    })
    console.log(data);
    try{
        const dataToSave = await data.save();
        res.status(200).json(dataToSave)
    }
    catch(error){
        res.status(400).json({message: error.message})
    }
};


exports.getAll = async (req, res) => {
    try{
        var url = "public/images/";
        const datas = await Content.find();
        for ( data of datas){
            console.log("data ",data);
            console.log("UrlImage ",data.urlImage);
            data.urlImage = imageHelper.base64_encode(url+data.urlImage);
        }
        res.json(datas)
    }
    catch(error){
        res.status(500).json({message: error.message})
    }
};

exports.getById = async (req, res) => {
    try{
        var url = "public/images/";
        const data = await Content.find({id:req.params.id});
        data.urlImage = imageHelper.base64_encode(url+data.urlImage);
        res.json(data)
    }
    catch(error){
        res.status(500).json({message: error.message})
    }
};
exports.getByFilter = async (req, res) => {
    try{
        const filters = req.query;
        var url = "public/images/";
        console.log(req.params)
        Content.find().or([
            {category:{$regex:'.*' +req.params.filter+'.*' ,$options:'i'}},
            {name:{$regex:'.*' + req.params.filter+'.*' ,$options:'i'}},
            {location:{$regex:'.*' +req.params.filter+'.*',$options:'i' }},
            {description:{$regex:'.*' +req.param.filter+'.*',$options:'i'}},
            {price:req.params.filter},
            {rating:req.params.filter}])
            .then(content => {
                for(c of content){
                    c.urlImage = imageHelper.base64_encode(url+c.urlImage);
                }
                res.send(content)
            });
        //res.json(data)
    }
    catch(error){
        res.status(500).json({message: error.message})
    }
};

exports.update = async (req, res) => {
    try {
        const updatedData = req.body;
        console.log(req.body.nom);
        await Content.findOneAndUpdate({id: req.params.id}, 
            {$set: 
                { 
                category: updatedData.category,
                name: updatedData.name,
                location: updatedData.location,
                description: updatedData.description,
                price: updatedData.price,
                rating: updatedData.rating,
                urlImage: updatedData.urlImage,
                }}, 
            {
                new: true,
            }, 
            (err, doc) => {
            if (err) {
                console.log("Something wrong when updating data!".err);
            }
            res.status(200).json({message: doc});
            console.log(doc);
        });
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }

};

