const User = require('../models/UserModels');
var admin = require("../utils/firebase");
const express = require('express')
const router = express.Router();
const bcrypt = require('bcrypt')
const userServices = require('../services/UserServices')

   

router.post('/login', (req, res, next) => {
    //console.log(req.body);
    const { login, mdp } = req.body;
    console.log( { login, mdp });
    userServices.login({login, mdp })
        .then(user => {
            res.json(user)
        }
        ).catch(err => next(err))
})

router.post('/register', (req, res, next) => {
    console.log(req.body);
    const { mdp } = req.body
    const salt = bcrypt.genSaltSync(10);
    req.body.mdp = bcrypt.hashSync(mdp, salt);

    userServices.register(req.body).then(
        () => {
            var topic = 'general'; 

            var message = {
            notification: {
                title: 'Bravo!',
                body: 'Vous êtes inscrits dans Dago!'
            },
            topic: topic
            };
            // Send a message to devices subscribed to the provided topic.
            admin.messaging().send(message)
            .then((response) => {
                // Response is a message ID string. 


                
                console.log('Successfully sent message:', response);
            })
            .catch((error) => {
                console.log('Error sending message:', error);
            });
            /////////////////
            
            res.status(200)
        }
    ).catch(
        err => next(err)
    )
})

router.get('/:id', (req, res, next) => {
    userServices.getById(req.params.id).then(
        (user) => res.json(user)
    ).catch(err => next(err))
})

router.post('/update', (req, res) => {
    try {
        const updatedData = req.body;
        console.log(req.body.nom);
        //console.log(req.params.idUser)
        User.findOneAndUpdate({idUser: req.body.idUser}, 
            {$set: 
                { 
                nom: updatedData.nom,
                prenom: updatedData.prenom,
                email: updatedData.email,
                }}, 
            {
                new: true,
            }, 
            (err, doc) => {
            if (err) {
                console.log("Something wrong when updating data!".err);
            }
            res.status(200).json({message: doc});
            console.log(doc);
        })
        console.log(User);
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }

})

module.exports = router;