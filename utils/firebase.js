////////////firebase
const admin = require("firebase-admin");
var serviceAccount = ("config/privateKey.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://prismappfcm.firebaseio.com"
});

module.exports = admin
